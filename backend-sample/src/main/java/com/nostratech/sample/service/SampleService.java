package com.nostratech.sample.service;

import com.nostratech.sample.vo.SampleVO;

public interface SampleService extends BaseService<SampleVO, String> {

}