package com.nostratech.sample.service.impl;

import com.nostratech.sample.converter.SampleVoConverter;
import com.nostratech.sample.exception.ServiceException;
import com.nostratech.sample.persistence.domain.Sample;
import com.nostratech.sample.persistence.repository.SampleRepository;
import com.nostratech.sample.service.SampleService;
import com.nostratech.sample.util.Constants;
import com.nostratech.sample.vo.SampleVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class SampleServiceImpl implements SampleService {

    public static final Logger logger = LoggerFactory.getLogger(SampleServiceImpl.class);

    @Autowired
    SampleVoConverter sampleVoConverter;

    @Autowired
    SampleRepository sampleRepository;

    @Override
    public SampleVO add(SampleVO vo) {

        Sample sample = sampleVoConverter.transferVOToModel(vo, null);

        sampleRepository.save(sample);

        vo.setId(sample.getSecureId());

        return vo;
    }

    @Override
    public SampleVO update(String sid, SampleVO vo) {

        Sample sample = sampleRepository.findBySecureId(sid);

        sample = sampleVoConverter.transferVOToModel(vo, sample);

        sampleRepository.saveAndFlush(sample);

        return sampleVoConverter.transferModelToVO(sample, null);

    }

    @Override
    public Boolean delete(String sid) {
        Sample sample = sampleRepository.findBySecureId(sid);

        if (sample == null) {
            throw new ServiceException("Sample not found: " + sid);
        }

        Integer id = sample.getId();

        sampleRepository.delete(id);

        if (!sampleRepository.exists(id)) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public Collection<SampleVO> findAll() {

        Collection<Sample> samples = sampleRepository.findAll();

        return sampleVoConverter.transferListOfModelToListOfVO(samples, null);
    }

    @Override
    public SampleVO findById(String sid) {

        Sample sample = sampleRepository.findBySecureId(sid);

        if (sample == null) {
            throw new ServiceException("Sample not found: " + sid);
        }

        return sampleVoConverter.transferModelToVO(sample, null);
    }

    @Override
    public Map<String, Object> search(Integer page, Integer limit) {


        Pageable pageable = new PageRequest(page, limit);
        Page<Sample> resultPage = sampleRepository.findAll(pageable);
        List<Sample> models = resultPage.getContent();
        Collection<SampleVO> vos = sampleVoConverter.transferListOfModelToListOfVO(models, null);

        return constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    protected Map<String, Object> constructMapReturn(Collection voList, long totalElements, int totalPages) {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put(Constants.PageParameter.LIST_DATA, voList);
        map.put(Constants.PageParameter.TOTAL_ELEMENTS, totalElements);
        map.put(Constants.PageParameter.TOTAL_PAGES, totalPages);

        return map;
    }

}
