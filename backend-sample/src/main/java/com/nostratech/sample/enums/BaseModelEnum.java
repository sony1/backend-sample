package com.nostratech.sample.enums;


/**
 * Base class untuk enum
 * <T> tipe dari internal valuenya
 * @author <a href="mailto:andri.khrisharyadi@nostratech.com">andri.khrisharyadi</a>
 */
public interface BaseModelEnum<T> {
	
	/**
	 * get internal value
	 */
	T getInternalValue();
}
